��          �      ,      �  ,   �     �  *   �  !   �       /   %     U  
   u  P   �  
   �     �  ]   �  Z   N  +   �     �     �  "  �  8        I  >   [     �     �  A   �  ,        >  R   K  
   �     �  n   �  b   -  +   �     �     �                                  
      	                                      Automatically flush OPcache after an upgrade Flush PHP OPcache Hide Flush PHP Opcache button in admin bar OPcache was successfully flushed. Settings saved. Sorry, you are not allowed to access this page. Sorry, you can't flush OPcache. Statistics This plugin allows to manage Zend OPcache inside your WordPress admin dashboard. WP OPcache WP OPcache Settings You do not have the Zend OPcache extension loaded, you need to install it to use this plugin. Zend OPcache is loaded but not activated. You need to set opcache.enable=1 in your php.ini http://wordpress.org/plugins/flush-opcache/ https://igln.fr/ nierdz PO-Revision-Date: 2020-02-23 13:42:02+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - WP OPcache - Stable (latest release)
 Purger automatiquement OPcache après une mise à niveau Purge PHP OPcache Masquer le bouton PHP Opcache dans la barre d’administration OPcache a bien été purgé. Réglages enregistrés. Désolé, vous n’êtes pas autorisé à accéder à cette page. Désolé, vous ne pouvez pas purger OPcache. Statistiques Cette extension permet de gérer Zend OPcache depuis le Tableau de bord WordPress. WP OPcache Réglages WP OPcache Vous n’avez pas l’extension Zend OPcache chargée, vous devez l’installer pour utiliser cette extension. Zend OPcache est chargé mais pas activé. Vous devez définir opcache.enable-1 dans votre php.ini http://wordpress.org/plugins/flush-opcache/ https://igln.fr/ nierdz 