<?php
session_start();
global $wpdb;
date_default_timezone_set("Asia/Kolkata"); 
$current_date_time=date("Y-m-d_H-i-s");
$cur_date = date('ymdhis');
$message="";
$careerid=$_GET['form'];

if($_SESSION['message']!=""){
	$message=$_SESSION['message'];
	unset($_SESSION['message']);
}
if (($_SERVER["REQUEST_METHOD"] == "POST") && $_POST['educationlevel']!="") {
	
		//careerform table	
		$career_table= $wpdb->prefix . "careers";
		$wpdb->update($career_table,array(
			'highesteducation' =>$_POST['educationlevel'],
			'technicalexpertise'=>implode(',',$_POST['workexp']),
			'technicalother'=>$_POST['otherworkexp'],
			'frontend'=>implode(',',$_POST['frontendtech']),
			'frontendother'=>$_POST['otherfrontend'],
			'backend'=>implode(',',$_POST['backendtech']),
			'backendother'=>$_POST['otherbackend'],
			'mobiletech'=>implode(',',$_POST['mobiletech']),
			'mobileother'=>$_POST['othermobile'],
			'design'=>implode(',',$_POST['designtech']),
			'designother'=>$_POST['otherdesign']),
			array('career_id'=>$careerid),		  
			array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),
			array('%d'));

			$_SESSION['message']="Thank You. Your submission has been sent!";
			/*send mail*/
			//mail to user
			$user =$_POST['careermail'];
			$subject ="BLVCK PiXEL : Career";
			$message ="<html><body><p>Hi,</p><p>Thank You. Your submission has been sent!</p></body></html>";

			$headers = array('Content-Type: text/html; charset=UTF-8');
			wp_mail($user, $subject, $message,$headers);

				//mail to admin
			$user ="bhagyashree.karwa@blvckpixel.com";
			$subject ="BLVCK PiXEL : Career";
			$message ="<html><body><p>Hi Admin,</p><p>There is new job application on website!</p></body></html>";

			$headers = array('Content-Type: text/html; charset=UTF-8');
			wp_mail($user, $subject, $message,$headers);
			$path=get_home_url().'/careers';	
				header("Location: $path");	
				
			
	}else{
	}
		
	if($message!=""){
			echo "<script>alert('".$message."');</script>";
	}
    ?>
    <form class="careerform careerformadvance form-validate" method="post" action="" enctype="multipart/form-data">
		<h3>Educational Information</h3>
		<div class="form-row">
			<div class="form-col">
				<label>Highest level of Education<span class="required"> *</span></label>
				<input type="radio" name="educationlevel" class="form-control first" value="masters">Masters            
				<input type="radio" name="educationlevel" class="form-control" value="bachelors">Bachelors            
				<input type="radio" name="educationlevel" class="form-control" value="school">School (12th Standard)            
			</div>
		</div>
		<h3>Work Experience</h3>
		
		<div class="form-row">
			<div class="form-col-full">
				<label>Please enter your primary technical expertise(You may select multiple options)<span class="required"> *</span></label>
				<div class="advancecheckbox">
					<span><input type="checkbox" name="workexp[]" class="form-control first" value="Fullstack">Fullstack</span>            
					<span><input type="checkbox" name="workexp[]" class="form-control" value="Fullstack with backend focus">Fullstack with backend focus</span>            
					<span><input type="checkbox" name="workexp[]" class="form-control" value="Fullstack with Frontend focus">Fullstack with Frontend focus</span>            
					<span><input type="checkbox" name="workexp[]" class="form-control" value="Frontend">Frontend</span>            
					<span><input type="checkbox" name="workexp[]" class="form-control first" value="Backend">Backend</span>            
					<span><input type="checkbox" name="workexp[]" class="form-control" value="Mobile App">Mobile App</span>            
					<span><input type="checkbox" name="workexp[]" class="form-control" value="Design">Design</span>            
					<span><input type="checkbox" name="workexp[]" class="form-control" value="Data Science">Data Science</span>            
					<span><input type="checkbox" name="workexp[]" class="form-control first" value="AR">AR</span>            
					<span><input type="checkbox" name="workexp[]" class="form-control" value="Machine Learning / AI">Machine Learning / AI</span>            
					<span><input type="checkbox" name="workexp[]" class="form-control" value="Blockchain">Blockchain</span>            
				</div>
				<input type="text" name="otherworkexp" class="form-control jsother" placeholder="other">
			</div>
		</div>
		<div class="form-row">
			<div class="form-col-full">
				<label>Front-End Technologies(Please select the technologies that you work in:)</span></label>
				<div class="advancecheckbox">
					<span><input type="checkbox" name="frontendtech[]" class="form-control first" value="None">None</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="WordPress">WordPress</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="HTML">HTML</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="CSS">CSS</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control first" value="Bootstrap">Bootstrap</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="JavaScript">JavaScript</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="AngularJS">AngularJS</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="Zepto.js">Zepto.js</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control first" value="jQuery">jQuery</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="Vue">Vue</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="Backbone">Backbone</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="Ember">Ember</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control first" value="UI/ UX Design">UI/ UX Design</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="SASS">SASS</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="ActionScript">ActionScript</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="CoffeeScript">CoffeeScript</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control first" value="VBScript">VBScript</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="Silverlight">Silverlight</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="Java (applets)">Java (applets)</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value=".NET">.NET</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control first" value="Semantic-UI">Semantic-UI</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="Foundation">Foundation</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="Materialise">Materialise</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="Backbone.js">Backbone.js</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control first" value="Express.js">Express.js</span>            
					<span><input type="checkbox" name="frontendtech[]" class="form-control" value="Ember.js">Ember.js</span>            
				</div>
				<input type="text" name="otherfrontend" class="form-control jsother" placeholder="other">				
			</div>
		</div>
		<div class="form-row">
			<div class="form-col-full">
				<label>Back-End Technologies(Please select the technologies that you work in:)</span></label>
				<div class="advancecheckbox">
					<span><input type="checkbox" name="backendtech[]" class="form-control first" value="None">None</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="Java">Java</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="PHP">PHP</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="Laravel">Laravel</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control first" value="Python (Django, Flask, Pylons)">Python (Django, Flask, Pylons)</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="Ruby">Ruby</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="C++">C++</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="Scala (Play)">Scala (Play)</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control first" value="Node.js">Node.js</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="MySQL">MySQL</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="PostgreSQL">PostgreSQL</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="MongoDB">MongoDB</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control first" value="Memcached">Memcached</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="Redis">Redis</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="Apache">Apache</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="Nginx">Nginx</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control first" value="JavaScript">JavaScript</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="CodeIgniter">CodeIgniter</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="Spring">Spring</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="C#">C#</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control first" value="REST">REST</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="GO">GO</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="Rails">Rails</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="Django">Django</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control first" value="JSP">JSP</span>            
					<span><input type="checkbox" name="backendtech[]" class="form-control" value="ASP">ASP</span> 
				</div>	
				<input type="text" name="otherbackend" class="form-control jsother" placeholder="other">
			</div>
		</div>
		<div class="form-row">
			<div class="form-col-full">
				<label>Mobile Technologies(Please select the technologies that you work in:)</span></label>
				<div class="advancecheckbox">
					<span><input type="checkbox" name="mobiletech[]" class="form-control first" value="None">None</span>            
					<span><input type="checkbox" name="mobiletech[]" class="form-control" value="Android">Android</span>            
					<span><input type="checkbox" name="mobiletech[]" class="form-control" value="iOS">iOS</span>            
					<span><input type="checkbox" name="mobiletech[]" class="form-control" value="React Native">React Native</span>            
					<span><input type="checkbox" name="mobiletech[]" class="form-control first" value="Flutter">Flutter</span>            
					<span><input type="checkbox" name="mobiletech[]" class="form-control" value="Kotlin">Kotlin</span>            
					<span><input type="checkbox" name="mobiletech[]" class="form-control" value="Swift">Swift</span>            
					<span><input type="checkbox" name="mobiletech[]" class="form-control" value="Pythin">Pythin</span>            
					<span><input type="checkbox" name="mobiletech[]" class="form-control first" value="Java">Java</span>            
					<span><input type="checkbox" name="mobiletech[]" class="form-control" value="R Programming">R Programming</span>            
				</div>
				<input type="text" name="othermobile" class="form-control jsother" placeholder="other">
			</div>
		</div>
		<div class="form-row">
			<div class="form-col-full">
				<label>Design(Which aspects of Design have you worked on?)</span></label>
				<div class="advancecheckbox">
					<span><input type="checkbox" name="designtech[]" class="form-control first" value="None">None</span>            
					<span><input type="checkbox" name="designtech[]" class="form-control" value="UX/UI">UX/UI</span>            
					<span><input type="checkbox" name="designtech[]" class="form-control" value="Graphics">Graphics</span>            
					<span><input type="checkbox" name="designtech[]" class="form-control" value="3D">3D</span>            
					<span><input type="checkbox" name="designtech[]" class="form-control first" value="Video & Animation">Video & Animation</span>           
				</div>
				<input type="text" name="otherdesign" class="form-control jsother" placeholder="other">
			</div>
		</div>
		<div class="form-row">
			<div class="form-col">
				<input type="submit" value="Submit" class="btn btn-success">
			</div>
        </div>
    </form>
