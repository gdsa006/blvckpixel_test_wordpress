<?php

function my_theme_enqueue_styles() {    
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );    
	wp_enqueue_style( 'child-style',get_stylesheet_directory_uri() . '/style.css', array( 'parent-style')    );
    wp_enqueue_style( 'wow-css', get_stylesheet_directory_uri(). '/css/animate/animate.min.css' );
	/*custom css*/

}
add_action( "wp_enqueue_scripts", "my_theme_enqueue_styles" );

function enque_scripts() {
   wp_enqueue_script('wow-js',get_stylesheet_directory_uri().'/js/wow/dist/wow.min.js', array(), '',true); 
	wp_enqueue_script('validatejs', get_stylesheet_directory_uri() . '/js/jquery.validate.min.js', array(), false, true);
   wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array(), true );
}

add_action('wp_footer', 'enque_scripts');
function add_customadmin_script() {
	$current_screen = get_current_screen();
	if($current_screen->base=="toplevel_page_careerdetails"){
		wp_enqueue_style('datatable-style', get_stylesheet_directory_uri() . '/js/datatables/datatables.min.css');
		wp_enqueue_style('datatable-buttons', get_stylesheet_directory_uri() . '/js/datatables/buttons.dataTables.min.css');
		wp_enqueue_script('datatable-script', get_stylesheet_directory_uri() . '/js/datatables/datatables.min.js', array(), 1.2, true);
	}
  wp_enqueue_script('admin-custom-script', get_stylesheet_directory_uri() . '/js/custom-admin.js', array(), 1.1, true);
  wp_enqueue_style( 'admin-css', get_stylesheet_directory_uri(). '/css/admincss.css' );
}

add_action('admin_enqueue_scripts', 'add_customadmin_script');

/*
By:Bhagyashree
Function For:custom career form
*/
function career_form_basic() {
	ob_start();
	require_once('career_form_basic.php');
	return ob_get_clean();	
} 
add_shortcode( 'career_form_basic', 'career_form_basic' );

/*
By:Bhagyashree
Function For:advance career form
*/
function career_form_advance() {
	ob_start();
	require_once('career_form_advance.php');
	return ob_get_clean();	
} 
add_shortcode( 'career_form_advance', 'career_form_advance' );

require_once get_stylesheet_directory() . '/career_details.php';

/*
By:Bhagyashree
Function For:Bookmarks list
*/
function my_bookmarks() {
	ob_start();
	require_once('my_bookmarks.php');
	return ob_get_clean();	
} 
add_shortcode( 'my_bookmarks', 'my_bookmarks' );


// black background for login, register and mybookmark pages


function blacken(){

	$the_id = get_the_id();

	if(($the_id == '2644') || ($the_id == '2646') || ($the_id == '2675') || ($the_id == '2652') ){
	
	echo "<style>
	
	#content{
	background-color: black;
	}
	
	</style>";
	
	}

}

add_action('wp_footer','blacken');




// start of @gagan - functions


// mailchimp api call to fetch data of a member @ gagan
function queryMailChimp($email){
	$username = "BLVCK PiXEL";
	//it can be anything    
 	$password = "33de8385f5ba4c8456989c5f6c93d31a";   
 	$subscriber_hash = md5($email);
	//  echo $subscriber_hash;
	// create curl resource     
 	$ch = curl_init();      
	 // set url      
 	curl_setopt($ch, CURLOPT_URL, "https://us6.api.mailchimp.com/3.0/lists/bae3455a87/members/{$subscriber_hash}"); 
	 //make sure your dc is correct     
	 //return the transfer as a string      
 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);     
 	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");    
	// $output contains the output string     
 	$output = curl_exec($ch);     
	// close curl resource to free up system resources     
 	curl_close($ch);       
 	return $output;   
  }



// add new tab - "unsubscribe_newsletter_tab" @ gagan

add_filter('um_account_page_default_tabs_hook', 'my_custom_tab_in_um', 100 );
function my_custom_tab_in_um( $tabs ) {
	$tabs[500]['unsubscribe_newsletters']['icon'] = 'um-faicon-stop';
	$tabs[500]['unsubscribe_newsletters']['title'] = 'Unsubscribe';
	$tabs[500]['unsubscribe_newsletters']['custom'] = true;
	return $tabs;
}
	


// make new tab[unsubscribe_newsletter_tab] hookable

add_action('um_account_tab__unsubscribe_newsletters', 'um_account_tab__unsubscribe_newsletters');
function um_account_tab__unsubscribe_newsletters( $info ) {
	global $ultimatemember;
	extract( $info );

	$output = $ultimatemember->account->get_tab_output('unsubscribe_newsletters');
	if ( $output ) { echo $output; }
}



// content for the new [unsubscribe_newsletter_tab] @ gagan

add_filter('um_account_content_hook_unsubscribe_newsletters', 'um_account_content_hook_unsubscribe_newsletters');
function um_account_content_hook_unsubscribe_newsletters( $output ){
	ob_start();
	?>
	<div class="um-field">
		<!-- tab content -->
		<?php
		$current_user = wp_get_current_user();
		echo "<input type='hidden' value='$current_user->user_email' id='hidden-email-unsubscribe'>";
		$data = queryMailChimp($current_user->user_email);
		$status_data = json_decode( $data, true );
		$status = $status_data['status'];
		// print_r($status);
		if($status == 'subscribed'){
			echo 'You are a Subscriber!';
			echo do_shortcode ("[mc4wp_form id='3445']");
		}
		else if($status == 'pending'){
			echo 'Your status is Pending! Please check your mailbox <span style="color: #a944fc">(' . $current_user->user_email . ')</span>';
		}
		else{
			echo 'Not a Subscriber! <a href="/blvckbook/" style="color: #a944fc">click here</a>';
		}
		// print_r($data);
		// $obj2 = json_decode( $data, true );

		// foreach ( $obj2['members'] as $coords => $street )
		// {   
		// 	$arr[] = $street['email_address'];
		// }
		// if( in_array( $current_user->user_email ,$arr ) )
		// {
		// 	echo do_shortcode ("[mc4wp_form id='3445']");
		// 	echo "You are a subscriber";
		// }else{
		// 	echo "not a subscriber! <a href='/blvckbook/' style='color: #a944fc'>click here</i>";
		// }
		// echo '<br>';
		// print_r($arr);
		?>
	</div>			
	<?php
	$output .= ob_get_contents();
	ob_end_clean();
	return $output;
}

// end of @gagan - functions




// changed metas of search page resultsQ
add_filter( 'astra_single_post_meta', 'custom_post_meta' );
add_filter( 'astra_blog_post_meta', 'custom_post_meta' );

function custom_post_meta( $old_meta )
{
 $post_meta = astra_get_option( 'blog-single-meta' );
 if ( !$post_meta ) return $old_meta;
 
 $post_meta = array('tag', 'category', 'date');
 $new_output = astra_get_post_meta( $post_meta, " | " );
 if ( !$new_output ) return $old_meta;
 
 return "<div class='entry-meta'>$new_output</div>";
}

/*DEFERRING AND ASYNCING RENDER-BLOCKING JAVASCRIPT*/
add_filter( 'script_loader_tag', 'defer_scripts', 10, 3 );
function defer_scripts( $tag, $handle, $src ) {
	// The handles of the enqueued scripts we want to defer
	$defer_scripts = array( 
		'ivory-search-scripts',
		'contact-form-7',
		'um_crop',
		'um_functions',
		'select2',
		'astra-theme-js',
		'addtoany',
		'favorites',
		'um_account',
		'um_profile',
		'um_fileupload',
		'um-gdpr',
	);
    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
    }
    
    return $tag;
}



// function enqueue_particlejs(){
// 	wp_enqueue_script('particle-js','https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.min.js');
//   }
//   add_action('wp_enqueue_scripts', 'enqueue_particlejs');

// ?>