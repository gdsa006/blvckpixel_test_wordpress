<?php
session_start();

global $wpdb;
date_default_timezone_set("Asia/Kolkata"); 
$current_date_time=date("Y-m-d_H-i-s");
$cur_date = date('ymdhis');
$message="";

if($_SESSION['message']!=""){
	$message=$_SESSION['message'];
	unset($_SESSION['message']);
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
		//careerform table	
		if($_FILES['cv']['name']!=""){
			
			$ext = pathinfo($_FILES['cv']['name'], PATHINFO_EXTENSION);
			$basePath = ABSPATH . 'cvs';
			if (!file_exists($basePath)) {
				mkdir($basePath, 0755, true);
			}
			$filename = "cv" . '_' . $cur_date . '.' . $ext;
			$file = $basePath . "/" . $filename;

			$isUploaded = copy($_FILES['cv']['tmp_name'], $file);
		}
		$career_table= $wpdb->prefix . "careers";
		if($isUploaded){
		$wpdb->insert($career_table,array(
			'career_name' =>$_POST['fullname'],
			'career_email' =>$_POST['careermail'],
			'career_phone'=>$_POST['phonenumber'],
			'ctc' =>$_POST['ctc'],
			'employmentmode'=>implode(',',$_POST['modeofemployment']),
			'availability'=>$_POST['availability'],
			'workexperience' =>$_POST['experience'],
			'cv'=>$filename,
			'register_on' =>$current_date_time),
			array('%s','%s','%s','%s','%s','%s','%s','%s','%s'));
			$lastid = $wpdb->insert_id;

			$_SESSION['message']="Thank You. Your submission has been sent!";
			/*send mail*/
			//mail to user
			$user =$_POST['careermail'];
			$subject ="BLVCK PiXEL : Career";
			$message ="<html><body><p>Hi,</p><p>Thank You. Your submission has been sent!</p></body></html>";

			$headers = array('Content-Type: text/html; charset=UTF-8');
			wp_mail($user, $subject, $message,$headers);

				//mail to admin
			$user ="bhagyashree.karwa@blvckpixel.com";
			$subject ="BLVCK PiXEL : Career";
			$message ="<html><body><p>Hi Admin,</p><p>There is new job application on website!</p></body></html>";

			$headers = array('Content-Type: text/html; charset=UTF-8');
			wp_mail($user, $subject, $message,$headers);
			if($_POST['submitform']=="current"){
				$path=get_home_url().'/careers';
			}else{
				$path=get_home_url().'/advance-career?form='.$lastid;			
			}
				header("Location: $path");	
				
		}else{	
			$_SESSION['message']="Something went wrong.Please try again";
		}
			
	}else{
	}
		
	if($message!=""){
			echo "<script>alert('".$message."');</script>";
	}
    ?>
    <form class="careerform form-validate" method="post" action="" enctype="multipart/form-data">
		<div class="form-row">
			<div class="form-col">
				<label>Full name (as per official records)<span class="required"> *</span></label>
				<input type="text" name="fullname" class="form-control" required="true" data-msg-required="Please enter full name">
			</div>
		</div>
		<div class="form-row">
			<div class="form-col">
				<label>Email<span class="required"> *</span></label>
				<input type="email" name="careermail" class="form-control" data-rule-required="true" data-msg-required="Please enter email" data-rule-email="true">
			</div>
			<div class="form-col">
				<label>Phone number with country code (eg: +91-9876543210) <span class="required"> *</span></label>
				<input type="text" name="phonenumber" class="form-control" required="true" data-msg-required="Please enter Phone number">
			</div>
		</div>
		<div class="form-row">
			<div class="form-col basiccheck">
				<label>Modes of employment you are comfortable working in.<span class="required"> *</span></label>
				<span><input type="checkbox" name="modeofemployment[]" class="form-control first" value="Full-time">Full-time </span>           
				<span><input type="checkbox" name="modeofemployment[]" class="form-control" value="Part-time">Part-time</span>            
				<span><input type="checkbox" name="modeofemployment[]" class="form-control" value="Freelance">Freelance</span>            
				<span><input type="checkbox" name="modeofemployment[]" class="form-control" value="Internship">Internship</span>            
			</div>
			<div class="form-col basiccheck">
				<label>Kindly mark your availability. <span class="required"> *</span></label>
				<span><input type="radio" name="availability" class="form-control first" value="Immediately">Immediately</span>            
				<span><input type="radio" name="availability" class="form-control" value="In 15 days">In 15 days</span>            
				<span><input type="radio" name="availability" class="form-control" value="In 1 month">In 1 month</span>            
				<span><input type="radio" name="availability" class="form-control" value="Later than 1 month">Later than 1 month</span>            
			</div>
		</div>
		<div class="form-row">
			<div class="form-col">
				<label>What is current CTC or last drawn CTC? Please mention annual salary and the currency.<span class="required"> *</span></label>
				<input type="text" name="ctc" class="form-control" required="true" data-msg-required="Please enter ctc">
			</div>
			<div class="form-col basiccheck">
				<label>No. of years of work experience <span class="required"> *</span></label>
				<span><input type="radio" name="experience" class="form-control first" value="0-2 years">0-2 years </span>           
				<span><input type="radio" name="experience" class="form-control" value="2-4 years">2-4 years</span>            
				<span><input type="radio" name="experience" class="form-control" value="4-6 years">4-6 years </span>           
				<span><input type="radio" name="experience" class="form-control" value="6-8 years">6-8 years</span>            
				<span><input type="radio" name="experience" class="form-control" value="8+ years">8+ years</span>            
			</div>
		</div>
		<div class="form-row">
			<div class="form-col">
				<label>Kindly attach your CV (doc or pdf formats)<span class="required"> *</span></label>
				<input type="file" name="cv" class="form-control" required="true" data-msg-required="Please attach your cv">
			</div>
			<div class="form-col">
				<label>Would you like to tell us more?<span class="required"> *</span></label>
				<div><input type="radio" name="moredetails" class="form-control jsmoredetails first" value="yes">Sure, I would like to give more details about my skills</div>          
				<div><input type="radio" name="moredetails" class="form-control jsmoredetails first" value="no" checked>No, I want to submit the form here</div>          
			</div>
		</div>
		<div class="form-row">
			<div class="form-col">
				<input type="hidden" name="submitform" value="current" class="jsSubmittype">
				<input type="submit" value="Submit" class="btn btn-success">
			</div>
        </div>
    </form>
