<?php
ob_start();
add_action('admin_menu', 'career_details' );
function career_details() {
  //create custom top-level menu
  add_menu_page( 'Career Details', 'Career Details','manage_options', 'careerdetails', 'careerdetails_callback','',26);
  add_submenu_page( 'Matrimony Profiles','View Details', 'View Details','manage_options', 'viewdetails', 'viewdetails_callback','',200);

}
/*user details listing page backend*/
function careerdetails_callback(){
  global $wpdb;
  $data=$wpdb->get_results("SELECT * FROM bp_careers order by career_id desc");
  ?>
  <div class="wrap">
    <h2>Career details</h2>
    <table class="wp-list-table widefat fixed striped"  id="dataTable">
      <thead>
        <tr>
          <th>Sr. No.</th>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Date</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if (!empty($data)) {
          $count=1;
          foreach ($data as $record) {
            ?>
            <tr>
              <td class="text-center"><?php echo $count++; ?></td>
              <td class="text-center"><?php echo $record->career_name; ?></td>
              <td class="text-center"><?php echo $record->career_email; ?></td>
              <td class="text-center"><?php echo $record->career_phone; ?></td>
              <td class="text-center"><?php echo date('d/m/Y',strtotime($record->register_on)); ?></td>
				<td class="text-center"><a href="<?php echo get_home_url() .'/cvs/'.$record->cv;?>" target="_blank" class="button button-primary">Download CV</a> <a href="<?php echo get_admin_url();?>admin.php?page=viewdetails&id=<?php echo $record->career_id;?>" class="button button-primary">View Details</a></td>
            </tr>
            <?php }
            }
            ?>
      </tbody>
    </table>
    
  </div>
  <?php
}
/* details page backend*/
function viewdetails_callback(){
	$careerid=$_GET['id'];
  global $wpdb;
  $data=$wpdb->get_results("SELECT * FROM bp_careers WHERE career_id=$careerid");
  if (!empty($data)) {
	  ?>
	<table class="table careerdata">
	<?php	
	foreach ($data as $record) {
		?>
		<tr>
			<td>Full name (as per official records)</td>
			<td><?php  echo $record->career_name;?></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><?php  echo $record->career_email;?></td>
		</tr>
		<tr>
			<td>Phone Number</td>
			<td><?php  echo $record->career_phone;?></td>
		</tr>
		<tr>
			<td>Modes of employment comfortable working in</td>
			<td><?php  echo $record->employmentmode;?></td>
		</tr>
		<tr>
			<td>Availability</td>
			<td><?php  echo $record->availability;?></td>
		</tr>
		<tr>
			<td>Current CTC or last drawn CTC</td>
			<td><?php  echo $record->ctc;?></td>
		</tr>
		<tr>
			<td>No. of years of work experience</td>
			<td><?php  echo $record->workexperience;?></td>
		</tr>
		<tr>
			<td>CV</td>
			<td><a href="<?php echo get_home_url() .'/cvs/'.$record->cv;?>" target="_blank" class="button button-primary">Download CV</a></td>
		</tr>
		<tr>
			<td>Highest level of Education</td>
			<td><?php echo $record->highesteducation; ?></td>
		</tr>
		<tr>
			<td>Primary technical expertise</td>
			<td><?php echo $record->technicalexpertise; ?></td>
		</tr>
		<tr>
			<td>Primary technical expertise (Other)</td>
			<td><?php echo $record->technicalother; ?></td>
		</tr>
		<tr>
			<td>Front-End Technologies</td>
			<td><?php echo $record->frontend; ?></td>
		</tr>
		<tr>
			<td>Front-End Technologies (Other)</td>
			<td><?php echo $record->frontendother; ?></td>
		</tr>
		<tr>
			<td>Back-End Technologies</td>
			<td><?php echo $record->backend; ?></td>
		</tr>
		<tr>
			<td>Back-End Technologies (Other)</td>
			<td><?php echo $record->backendother; ?></td>
		</tr>
		<tr>
			<td>Mobile Technologies</td>
			<td><?php echo $record->highesteducation; ?></td>
		</tr>
		<tr>
			<td>Mobile Technologies (Other)</td>
			<td><?php echo $record->mobileother; ?></td>
		</tr>
		<tr>
			<td>Aspects of Design worked on</td>
			<td><?php echo $record->design; ?></td>
		</tr>
		<tr>
			<td>Aspects of Design worked on (Other)</td>
			<td><?php echo $record->designother; ?></td>
		</tr>

		<?php
	}
	  ?>
	  </table>
<?php
  }  
}
