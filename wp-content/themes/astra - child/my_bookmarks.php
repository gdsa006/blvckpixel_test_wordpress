<?php
if(is_user_logged_in()){
	$userid=get_current_user_id();
	$favoritelist=get_user_favorites($user_id = $userid, $site_id = null, $filters = null);
	if(sizeof($favoritelist)>0){
		foreach($favoritelist as $fav){
			$postid=$fav;
			$posttitle=get_the_title($postid);
			$postimg=get_the_post_thumbnail($postid);
			$posturl=get_the_permalink($postid);
			//$postexcerpt=get_the_excerpt($postid);
			$post=get_post($postid);
			$content = apply_filters('the_content', $post->post_content); 
            $content = substr(strip_tags($content),0,180); 
			$postdate=get_the_date('d/m/Y', $postid);
			$author_id = get_post_field( 'post_author', $postid );
			$author_name = get_the_author_meta('user_nicename', $author_id);
			?>
			<div class='bookmarkslist'> 
				<i class="fas fa-bookmark"></i>
				<div id='col-one' style='background-image:url("<?php echo $postimg; ?>");'>
								<?php echo $postimg; ?>
				<div class="bk-button" >
					<?php // the_favorites_button($postid, $site_id);?>
				</div>	
				</div>
				<div id='col-two'><h3> 
					<a href="<?php echo $posturl; ?>" style="font-weight: bold"> <?php echo $posttitle;?></a> 
				</h3>
					<p><?php echo $content; ?>...<br><a href="<?php echo $posturl; ?>" class="bk_readmore">Read More</a></p>
				</div>
				
			</div> 
		<?php
		}
	}
}
?>