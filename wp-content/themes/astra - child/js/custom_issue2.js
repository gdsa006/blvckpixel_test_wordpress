jQuery(document).ready(function($) {
$('.custom-logo-slider .ha-logo-grid-wrapper').addClass('owl-carousel').addClass('owl-carousel-logo').addClass('owl-theme');
$('.custom-logo-slider .ha-logo-grid-wrapper .ha-logo-grid-item').addClass('item');
//$('.custom-logo-slider .ha-logo-grid-wrapper').children().wrapAll("<div class='item'></div>");
});



$(function() {
  // Owl Carousel
  var owl = $(".owl-carousel"); owl.owlCarousel({ items: 2, margin: 10, loop: true, nav: false
  });
});




























// page loading anmation
var link = document.getElementById('pageloader');

$(window).load(function() {
	link.style.visibility = 'hidden';
});

// menu hide active flag from dropdown @ gagan
jQuery(document).ready(function($) {
	
$(".main-header-menu-toggle.ast-mobile-menu-trigger-fill").on('click', function(event) {
		$('.pll-parent-menu-item .sub-menu').css('display', 'block');
});
if (jQuery(window).width() < 922) {
console.log('leff 922');
	$('.pll-parent-menu-item').addClass('ast-submenu-expanded');
	$('.pll-parent-menu-item .sub-menu').css('display', 'block');

	$('.pll-parent-menu-item button.ast-menu-toggle').remove();
	$('.pll-parent-menu-item > a.menu-link').hide();
	$('.pll-parent-menu-item .sub-menu .lang-item-en').show();

	if(document.documentElement.lang == 'en-US'){
		$('.pll-parent-menu-item .sub-menu .lang-item-en').remove();
		$('.pll-parent-menu-item .sub-menu .lang-item-fr').attr('style', 'border-top: solid 1px; border-bottom: solid 1px');
		$('#lang-text').remove();
		$('.pll-parent-menu-item .sub-menu .lang-item-fr a').prepend('<span id="lang-text" style="text-transform: none; position: relative; top: 2px;">View in French </span>');
	}else{
		$('.pll-parent-menu-item .sub-menu .lang-item-fr').remove();
		$('.pll-parent-menu-item .sub-menu .lang-item-en').attr('style', 'border-top: solid 1px; border-bottom: solid 1px');
		$('#lang-text').remove();
		$('.pll-parent-menu-item .sub-menu .lang-item-en a').prepend('<span id="lang-text" style="text-transform: none; position: relative; top: 2px;">View in English </span>');
	}
}else{
	console.log('more 922');

	if(document.documentElement.lang == 'en-US'){
		$('.pll-parent-menu-item .sub-menu .lang-item-en').hide();
	}else{
		$('.pll-parent-menu-item .sub-menu .lang-item-fr').hide();
	}
}
});


//animated background particles system
/*
 * @author  - @sebagarcia
 * @version 1.1.0
 * Inspired & based on "Particleground" by Jonathan Nicol
 */

;(function(window, document) {
	"use strict";
	var pluginName = 'particleground';
  
	// http://youmightnotneedjquery.com/#deep_extend
	function extend(out) {
	  out = out || {};
	  for (var i = 1; i < arguments.length; i++) {
		var obj = arguments[i];
		if (!obj) continue;
		for (var key in obj) {
		  if (obj.hasOwnProperty(key)) {
			if (typeof obj[key] === 'object')
			  deepExtend(out[key], obj[key]);
			else
			  out[key] = obj[key];
		  }
		}
	  }
	  return out;
	};
  
	var $ = window.jQuery;
  
	function Plugin(element, options) {
	  var canvasSupport = !!document.createElement('canvas').getContext;
	  var canvas;
	  var ctx;
	  var particles = [];
	  var raf;
	  var mouseX = 0;
	  var mouseY = 0;
	  var winW;
	  var winH;
	  var desktop = !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|BB10|mobi|tablet|opera mini|nexus 7)/i);
	  var orientationSupport = !!window.DeviceOrientationEvent;
	  var tiltX = 0;
	  var pointerX;
	  var pointerY;
	  var tiltY = 0;
	  var paused = false;
  
	  options = extend({}, window[pluginName].defaults, options);
  
	  /**
	   * Init
	   */
	  function init() {
		if (!canvasSupport) { return; }
  
		//Create canvas
		canvas = document.createElement('canvas');
		canvas.className = 'pg-canvas';
		canvas.style.display = 'block';
		element.insertBefore(canvas, element.firstChild);
		ctx = canvas.getContext('2d');
		styleCanvas();
  
		// Create particles
		var numParticles = Math.round((canvas.width * canvas.height) / options.density);
		for (var i = 0; i < numParticles; i++) {
		  var p = new Particle();
		  p.setStackPos(i);
		  particles.push(p);
		};
  
		window.addEventListener('resize', function() {
		  resizeHandler();
		}, false);
  
		document.addEventListener('mousemove', function(e) {
		  mouseX = e.pageX;
		  mouseY = e.pageY;
		}, false);
  
		if (orientationSupport && !desktop) {
		  window.addEventListener('deviceorientation', function () {
			// Contrain tilt range to [-30,30]
			tiltY = Math.min(Math.max(-event.beta, -30), 30);
			tiltX = Math.min(Math.max(-event.gamma, -30), 30);
		  }, true);
		}
  
		draw();
		hook('onInit');
	  }
  
	  /**
	   * Style the canvas
	   */
	  function styleCanvas() {
		canvas.width = element.offsetWidth;
		canvas.height = element.offsetHeight;
		ctx.fillStyle = options.dotColor;
		ctx.strokeStyle = options.lineColor;
		ctx.lineWidth = options.lineWidth;
	  }
  
	  /**
	   * Draw particles
	   */
	  function draw() {
		if (!canvasSupport) { return; }
  
		winW = window.innerWidth;
		winH = window.innerHeight;
  
		// Wipe canvas
		ctx.clearRect(0, 0, canvas.width, canvas.height);
  
		// Update particle positions
		for (var i = 0; i < particles.length; i++) {
		  particles[i].updatePosition();
		};
		// Draw particles
		for (var i = 0; i < particles.length; i++) {
		  particles[i].draw();
		};
  
		// Call this function next time screen is redrawn
		if (!paused) {
		  raf = requestAnimationFrame(draw);
		}
	  }
  
	  /**
	   * Add/remove particles.
	   */
	  function resizeHandler() {
		// Resize the canvas
		styleCanvas();
  
		var elWidth = element.offsetWidth;
		var elHeight = element.offsetHeight;
  
		// Remove particles that are outside the canvas
		for (var i = particles.length - 1; i >= 0; i--) {
		  if (particles[i].position.x > elWidth || particles[i].position.y > elHeight) {
			particles.splice(i, 1);
		  }
		};
  
		// Adjust particle density
		var numParticles = Math.round((canvas.width * canvas.height) / options.density);
		if (numParticles > particles.length) {
		  while (numParticles > particles.length) {
			var p = new Particle();
			particles.push(p);
		  }
		} else if (numParticles < particles.length) {
		  particles.splice(numParticles);
		}
  
		// Re-index particles
		for (i = particles.length - 1; i >= 0; i--) {
		  particles[i].setStackPos(i);
		};
	  }
  
	  /**
	   * Pause particle system
	   */
	  function pause() {
		paused = true;
	  }
  
	  /**
	   * Start particle system
	   */
	  function start() {
		paused = false;
		draw();
	  }
  
	  /**
	   * Particle
	   */
	  function Particle() {
		this.stackPos;
		this.active = true;
		this.layer = Math.ceil(Math.random() * 3);
		this.parallaxOffsetX = 0;
		this.parallaxOffsetY = 0;
		// Initial particle position
		this.position = {
		  x: Math.ceil(Math.random() * canvas.width),
		  y: Math.ceil(Math.random() * canvas.height)
		}
		// Random particle speed, within min and max values
		this.speed = {}
		switch (options.directionX) {
		  case 'left':
			this.speed.x = +(-options.maxSpeedX + (Math.random() * options.maxSpeedX) - options.minSpeedX).toFixed(2);
			break;
		  case 'right':
			this.speed.x = +((Math.random() * options.maxSpeedX) + options.minSpeedX).toFixed(2);
			break;
		  default:
			this.speed.x = +((-options.maxSpeedX / 2) + (Math.random() * options.maxSpeedX)).toFixed(2);
			this.speed.x += this.speed.x > 0 ? options.minSpeedX : -options.minSpeedX;
			break;
		}
		switch (options.directionY) {
		  case 'up':
			this.speed.y = +(-options.maxSpeedY + (Math.random() * options.maxSpeedY) - options.minSpeedY).toFixed(2);
			break;
		  case 'down':
			this.speed.y = +((Math.random() * options.maxSpeedY) + options.minSpeedY).toFixed(2);
			break;
		  default:
			this.speed.y = +((-options.maxSpeedY / 2) + (Math.random() * options.maxSpeedY)).toFixed(2);
			this.speed.x += this.speed.y > 0 ? options.minSpeedY : -options.minSpeedY;
			break;
		}
	  }
  
	  /**
	   * Draw particle
	   */
	  Particle.prototype.draw = function() {
		// Draw circle
		ctx.beginPath();
		ctx.arc(this.position.x + this.parallaxOffsetX, this.position.y + this.parallaxOffsetY, options.particleRadius / 2, 0, Math.PI * 2, true);
		ctx.closePath();
		ctx.fill();
  
		// Draw lines
		ctx.beginPath();
		// Iterate over all particles which are higher in the stack than this one
		for (var i = particles.length - 1; i > this.stackPos; i--) {
		  var p2 = particles[i];
  
		  // Pythagorus theorum to get distance between two points
		  var a = this.position.x - p2.position.x
		  var b = this.position.y - p2.position.y
		  var dist = Math.sqrt((a * a) + (b * b)).toFixed(2);
  
		  // If the two particles are in proximity, join them
		  if (dist < options.proximity) {
			ctx.moveTo(this.position.x + this.parallaxOffsetX, this.position.y + this.parallaxOffsetY);
			if (options.curvedLines) {
			  ctx.quadraticCurveTo(Math.max(p2.position.x, p2.position.x), Math.min(p2.position.y, p2.position.y), p2.position.x + p2.parallaxOffsetX, p2.position.y + p2.parallaxOffsetY);
			} else {
			  ctx.lineTo(p2.position.x + p2.parallaxOffsetX, p2.position.y + p2.parallaxOffsetY);
			}
		  }
		}
		ctx.stroke();
		ctx.closePath();
	  }
  
	  /**
	   * update particle position
	   */
	  Particle.prototype.updatePosition = function() {
		if (options.parallax) {
		  if (orientationSupport && !desktop) {
			// Map tiltX range [-30,30] to range [0,winW]
			var ratioX = (winW - 0) / (30 - -30);
			pointerX = (tiltX - -30) * ratioX + 0;
			// Map tiltY range [-30,30] to range [0,winH]
			var ratioY = (winH - 0) / (30 - -30);
			pointerY = (tiltY - -30) * ratioY + 0;
		  } else {
			pointerX = mouseX;
			pointerY = mouseY;
		  }
		  // Calculate parallax offsets
		  this.parallaxTargX = (pointerX - (winW / 2)) / (options.parallaxMultiplier * this.layer);
		  this.parallaxOffsetX += (this.parallaxTargX - this.parallaxOffsetX) / 10; // Easing equation
		  this.parallaxTargY = (pointerY - (winH / 2)) / (options.parallaxMultiplier * this.layer);
		  this.parallaxOffsetY += (this.parallaxTargY - this.parallaxOffsetY) / 10; // Easing equation
		}
  
		var elWidth = element.offsetWidth;
		var elHeight = element.offsetHeight;
  
		switch (options.directionX) {
		  case 'left':
			if (this.position.x + this.speed.x + this.parallaxOffsetX < 0) {
			  this.position.x = elWidth - this.parallaxOffsetX;
			}
			break;
		  case 'right':
			if (this.position.x + this.speed.x + this.parallaxOffsetX > elWidth) {
			  this.position.x = 0 - this.parallaxOffsetX;
			}
			break;
		  default:
			// If particle has reached edge of canvas, reverse its direction
			if (this.position.x + this.speed.x + this.parallaxOffsetX > elWidth || this.position.x + this.speed.x + this.parallaxOffsetX < 0) {
			  this.speed.x = -this.speed.x;
			}
			break;
		}
  
		switch (options.directionY) {
		  case 'up':
			if (this.position.y + this.speed.y + this.parallaxOffsetY < 0) {
			  this.position.y = elHeight - this.parallaxOffsetY;
			}
			break;
		  case 'down':
			if (this.position.y + this.speed.y + this.parallaxOffsetY > elHeight) {
			  this.position.y = 0 - this.parallaxOffsetY;
			}
			break;
		  default:
			// If particle has reached edge of canvas, reverse its direction
			if (this.position.y + this.speed.y + this.parallaxOffsetY > elHeight || this.position.y + this.speed.y + this.parallaxOffsetY < 0) {
			  this.speed.y = -this.speed.y;
			}
			break;
		}
  
		// Move particle
		this.position.x += this.speed.x;
		this.position.y += this.speed.y;
	  }
  
	  /**
	   * Setter: particle stacking position
	   */
	  Particle.prototype.setStackPos = function(i) {
		this.stackPos = i;
	  }
  
	  function option (key, val) {
		if (val) {
		  options[key] = val;
		} else {
		  return options[key];
		}
	  }
  
	  function destroy() {
		console.log('destroy');
		canvas.parentNode.removeChild(canvas);
		hook('onDestroy');
		if ($) {
		  $(element).removeData('plugin_' + pluginName);
		}
	  }
  
	  function hook(hookName) {
		if (options[hookName] !== undefined) {
		  options[hookName].call(element);
		}
	  }
  
	  init();
  
	  return {
		option: option,
		destroy: destroy,
		start: start,
		pause: pause
	  };
	}
  
	window[pluginName] = function(elem, options) {
	  return new Plugin(elem, options);
	};
  
	window[pluginName].defaults = {
	  minSpeedX: 1,
	  maxSpeedX: 1.5,
	  minSpeedY: 1,
	  maxSpeedY: 1.5,
	  directionX: 'center', // 'center', 'left' or 'right'. 'center' = dots bounce off edges
	  directionY: 'left', // 'center', 'up' or 'down'. 'center' = dots bounce off edges
	  density: 9000, // How many particles will be generated: one particle every n pixels
	  dotColor: '#000000',
	  lineColor: '#000000',
	  particleRadius: 8, // Dot size
	  lineWidth: 1,
	  curvedLines: false,
	  proximity: 130, // How close two dots need to be before they join
	  parallax: false,
	  parallaxMultiplier: 5, // The lower the number, the more extreme the parallax effect
	  onInit: function() {},
	  onDestroy: function() {}
	};
  
	// nothing wrong with hooking into jQuery if it's there...
	if ($) {
	  $.fn[pluginName] = function(options) {
		if (typeof arguments[0] === 'string') {
		  var methodName = arguments[0];
		  var args = Array.prototype.slice.call(arguments, 1);
		  var returnVal;
		  this.each(function() {
			if ($.data(this, 'plugin_' + pluginName) && typeof $.data(this, 'plugin_' + pluginName)[methodName] === 'function') {
			  returnVal = $.data(this, 'plugin_' + pluginName)[methodName].apply(this, args);
			}
		  });
		  if (returnVal !== undefined){
			return returnVal;
		  } else {
			return this;
		  }
		} else if (typeof options === "object" || !options) {
		  return this.each(function() {
			if (!$.data(this, 'plugin_' + pluginName)) {
			  $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
			}
		  });
		}
	  };
	}
  
  })(window, document);
  

  (function() {
	  var lastTime = 0;
	  var vendors = ['ms', 'moz', 'webkit', 'o'];
	  for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
								   || window[vendors[x]+'CancelRequestAnimationFrame'];
	  }
  
	  if (!window.requestAnimationFrame)
		window.requestAnimationFrame = function(callback, element) {
		  var currTime = new Date().getTime();
		  var timeToCall = Math.max(0, 16 - (currTime - lastTime));
		  var id = window.setTimeout(function() { callback(currTime + timeToCall); },
			timeToCall);
		  lastTime = currTime + timeToCall;
		  return id;
		};
  
	  if (!window.cancelAnimationFrame)
		window.cancelAnimationFrame = function(id) {
		  clearTimeout(id);
		};
  }());
  
  document.addEventListener('DOMContentLoaded', function () {
	particleground(document.getElementById('particles'), {
	  dotColor: '#ddd',
	  lineColor: '#ddd'
	});
	var intro = document.getElementById('intro');
	intro.style.marginTop = - intro.offsetHeight / 2 + 'px';
  }, false);
  


// search design
$("#menu-item-5916 .menu-link").on('click', function(event) {
	$(this).html("");
	var home_url = '<?php echo home_url() ?>';
	$("#menu-item-5916").html("<form class='searh' method='get' action=" + home_url + " role='search'><input type='text' id='search-box'></form>");
	$("#menu-item-5916 #search-box").attr('style', 'padding: 5px 2px; border-radius: 0; opacity: 0');
	$("#menu-item-5916 #search-box").attr('placeholder', 'Search');
	$("#menu-item-5916 #search-box").animate({opacity: 1}, {duration: 1000});
 });


/*  search input repositioning @ gagan */
$(window).on('load resize', function () {
	if (jQuery(window).width() > 922) {
		$( "nav .is-search-form input" ).attr("style", "padding: 3px 0; line-height: 40px; font-size: 25px !important; background: #000; color: #fff; border: 0 !important;width: 90%; height: auto");
		$( "nav .is-search-submit" ).hide();
		$( "nav .is-search-form" ).hide();
		$( "nav .is-search-form" ).clone().appendTo( ".main-header-bar-navigation" );
		$( "nav .is-search-form" ).remove();
		$( ".main-header-bar-navigation .is-search-form label" ).append('<span id="close-search" style="color: #7d7d7d; width: 2%; font-weight: bold; font-family: cursive;">X</span>');
		$(".main-header-bar-navigation #close-search").hover(function(){
			$(this).attr('style', 'cursor: pointer !important; color: #fafafa; font-family: cursive; font-weight: bold');
		}, function(){
			$(this).attr('style', 'color: #7d7d7d; width: 2%; font-weight: bold; font-family: cursive;');
		});
		$( ".main-header-bar-navigation .is-search-form" ).css('margin','35px 0');

		$(".main-header-bar-navigation .is-screen-reader-text").attr("style", "display: inline-block; float: left; width: 8%; padding: 13px 0; height: auto; margin: auto; position: relative !important; clip-path: revert; background: #000; text-align: center");
		$svg = '<svg width="20" height="20" class="search-icon" role="img" viewBox="2 9 20 5" focusable="false" aria-label="Search" style="background-color: transparent; padding: unset !important;"><path class="search-icon-path" d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" style="fill: #7d7d7d !important"></path></svg>';
		$(".main-header-bar-navigation .is-screen-reader-text").html($svg);
		$( "nav .astm-search-menu a svg" ).on( "click", function() {
			$( "body" ).append("<div id='overlay' style='position: absolute; left: 0; top: 0; width: 100%; height: 100%; background: #000000de; opacity: 1; backdrop-filter: blur(1px); z-index: 9'></div>");	
			$( "header nav" ).fadeIn( "slow", function() {
				$( this ).attr("style", "height: 0; overflow: hidden;");
			});
			$( ".main-header-bar-navigation .is-search-form" ).fadeIn( "slow", function() {
				$( ".is-search-form" ).show();
			});
		document.getElementById("is-search-input-0").focus();
		});
		$('#close-search').on('keypress click', function(event) {
			$( "header nav" ).show().delay(100).fadeIn('slow').removeAttr("style");
			$( ".main-header-bar-navigation .is-search-form" ).hide().delay(100).fadeOut('slow');
			$( "body #overlay" ).remove();
			
			if (event.key == 'Escape') { 
				$( "header nav" ).show().delay(100).fadeIn('slow').removeAttr("style");
				$( ".main-header-bar-navigation .is-search-form" ).hide().delay(100).fadeOut('slow');
				$( "body #overlay" ).remove();
			}
		});
	}
});


$.fn.toggleAttr = function(attr, val) {
    var test = $(this).attr(attr);
    if ( test ) { 
      // if attrib exists with ANY value, still remove it
      $(this).removeAttr(attr);
    } else {
      $(this).attr(attr, val);
    }
    return this;
  };

// add search in mobile menu
$(function () {
	$mobileSearchButton = '<div class="astm-search-menu1 is-menu1 is-dropdown1" id="mobile-search-wrapper"><a href="javascript:void(0)" aria-label="Search Icon Link" class="menu-link mobile-search-link" style="line-height: inherit !important"><svg width="20" height="20" class="search-icon" role="img" viewBox="2 9 20 5" focusable="false" aria-label="Search" style="background-color: transparent; padding: unset !important; top: 4px; position: relative; left: -6px;"><path class="search-icon-path" d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" style="fill: #ffffff !important"></path></svg></a></div>';
	$mobileSearchForm = '<form class="is-searchform is-form-style is-form-style-3 is-form-id-0 mobile" style="z-index: 999" action="/" method="get" role="search" style="display: block;"><label for="is-search-input-0"><span class="is-screen-reader-text">Search for:</span><input type="search" id="is-search-input-0" name="s" value="" class="is-search-input" placeholder="Search here..." autocomplete="off"></label><button type="submit" class="is-search-submit"><span class="is-screen-reader-text">Search Button</span><span class="is-search-icon"><svg focusable="false" aria-label="Search" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24px"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path></svg></span></button></form>';
	$( "#ast-mobile-header .site-header-primary-section-right" ).append($mobileSearchButton);
	$( "header.site-header" ).append($mobileSearchForm);
	$( ".is-searchform.mobile" ).css("z-index", "999");
	$( ".is-searchform" ).attr('style', 'display: none');	

	$("#mobile-search-wrapper").on('click', 'a', function() {
		$( ".is-searchform" ).toggle();	
		document.getElementById("is-search-input-0").focus();

		$( "#mobile-search-wrapper" ).toggleClass('active');	
	});
});


/*  underline animation @ gagan */
function isVisible($el) {
  var winTop = $(window).scrollTop();
  var winBottom = winTop + $(window).height();
  var elTop = $el.offset().top;
  var elBottom = elTop + $el.height();
  return ((elBottom<= winBottom) && (elTop >= winTop));
}


// BlvckBook underline animations
$(function() {
  var lastScrollTop = $('.belowLineAnimationsActivater').offset().top;
  $(window).scroll(function() {

	   var st = $(this).scrollTop();
    
		if($("body").hasClass("page-id-396")){
			if (st > lastScrollTop){
				if(isVisible($(".elementor-element-0edca09"))){
					$(".page-id-396 .elementor-element-0edca09 h2").addClass("animateMe");
				}
				else if(isVisible($(".elementor-element-b957bd8"))){
					$(".page-id-396 .elementor-widget-heading h2").removeClass("animateMe");
					$(".page-id-396 .elementor-element-b957bd8 h2").addClass("animateMe");
				}
				else if(isVisible($(".elementor-element-8448b3c"))){
					$(".page-id-396 .elementor-widget-heading h2").removeClass("animateMe");
					$(".page-id-396 .elementor-element-8448b3c h2").addClass("animateMe");
				}
				else if(isVisible($(".elementor-element-f5629c6"))){
					$(".page-id-396 .elementor-widget-heading h2").removeClass("animateMe");
					$(".page-id-396 .elementor-element-f5629c6 h2").addClass("animateMe");
				}
				else if(isVisible($(".elementor-element-f03efaa"))){
					$(".page-id-396 .elementor-widget-heading h2").removeClass("animateMe");
					$(".page-id-396 .elementor-element-f03efaa h2").addClass("animateMe");
				}
				else if (isVisible($(".elementor-element-0dedc6b"))){
					$(".page-id-396 .elementor-widget-heading h2").removeClass("animateMe");
					$(".page-id-396 .elementor-element-0dedc6b h2").addClass("animateMe");
				}
			}
			else{
				$(".elementor-widget-heading h2").removeClass("animateMe");
			}
		}
		else if($("body").hasClass("page-id-394")){
			console.log('394');
			if (st > lastScrollTop){
				if(isVisible($(".elementor-element-56f561d"))){
					$(".elementor-widget-heading h3").removeClass("animateMe");
					$(".elementor-element-56f561d h3").addClass("animateMe");
				}
			}
			else{
				$(".elementor-widget-heading h3").removeClass("animateMe");
			}
		}
		else if($("body").hasClass("page-id-2477")){
			// console.log('2477');
			if (st > lastScrollTop){
				if(isVisible($(".elementor-element-88d65df"))){
					$(".elementor-widget-heading h3").removeClass("animateMe");
					$(".elementor-element-88d65df h3").addClass("animateMe");
				}
			}
			else{
				$(".elementor-widget-heading h3").removeClass("animateMe");
			}
		}
		else if($("body").hasClass("page-id-888")){
			// console.log('2477');
			if (st > lastScrollTop){
				if(isVisible($(".elementor-element-56f561d"))){
					$(".elementor-widget-heading h3").removeClass("animateMe");
					$(".elementor-element-56f561d h3").addClass("animateMe");
				}
			}
			else{
				$(".elementor-widget-heading h3").removeClass("animateMe");
			}
		}
		else if($("body").hasClass("page-id-6137")){ // services page - fr
			// console.log('2477');
			if (st > lastScrollTop){
				if(isVisible($(".elementor-element-778948c"))){
					$(".elementor-widget-heading h3").removeClass("animateMe");
					$(".elementor-element-778948c h3").addClass("animateMe");
				}
			}
			else{
				$(".elementor-widget-heading h3").removeClass("animateMe");
			}
		}
		else if($("body").hasClass("page-id-6093")){ // approach page - fr
			// console.log('2477');
			if (st > lastScrollTop){
				if(isVisible($(".elementor-element-86ba7e0"))){
					$(".elementor-widget-heading h3").removeClass("animateMe");
					$(".elementor-element-86ba7e0 h3").addClass("animateMe");
				}
			}
			else{
				$(".elementor-widget-heading h3").removeClass("animateMe");
			}
		}
		else if($("body").hasClass("page-id-1003")){ // approach page - fr
			// console.log('2477');
			if (st > lastScrollTop){
				if(isVisible($(".elementor-element-1f6be45"))){
					$(".elementor-widget-heading h3").removeClass("animateMe");
					$(".elementor-element-1f6be45 h3").addClass("animateMe");
				}
			}
			else{
				$(".elementor-widget-heading h3").removeClass("animateMe");
			}
		}
		else if($("body").hasClass("page-id-6273")){ // approach page - fr
			// console.log('2477');
			if (st > lastScrollTop){
				if(isVisible($(".elementor-element-84a22b5"))){
					$(".elementor-widget-heading h3").removeClass("animateMe");
					$(".elementor-element-84a22b5 h3").addClass("animateMe");
				}
			}
			else{
				$(".elementor-widget-heading h3").removeClass("animateMe");
			}
		}












                          else if($("body").hasClass("page-id-888")){ // about-page - en
        // console.log('2477');
if (st > lastScrollTop){ if(isVisible($(".elementor-element-961e8b1"))){ $(".elementor-widget-heading h3").removeClass("animateMe"); 
$(".elementor-element-56f561d h3").addClass("animateMe");
}
} else{ $(".elementor-widget-heading h3").removeClass("animateMe"); } }





























		else if($("body").hasClass("page-id-6104")){ // about page - fr
}			// console.log('2477');
else{ if (st > lastScrollTop){ if(isVisible($(".elementor-element-9c5c285"))){ $(".elementor-widget-heading 
h3").removeClass("animateMe"); $(".elementor-widget-heading h3").removeClass("animateMe");
}					$(".elementor-element-9c5c285 h3").addClass("animateMe");
}				}
			}
			else{ $(".elementor-widget-heading h3").removeClass("animateMe");
			}
		}				
  });
});


/* homepage animation @ gagan */
jQuery(document).ready(function($) {
    var x = 1;
	const kwds1 = ["TECHNOLOGY", "INNOVATION", "BUSINESS"];
    const kwds2 = ["PEOPLE", "EXPERIENCE", "DESIGN"];
	const imgs1 = ["technology1", "innovation1", "business1", "people2" , "experience2" , "design2"];
	const imgs2 = ["people1", "experience1", "design1" , "technology2" , "innovation2"];
    function aniMate(x){
        if(x){
				var keyword1 = "<h1>" + kwds1[Math.floor(Math.random() * kwds1.length)] + "</h1>";
                $('.custom-animation-1.first div div:nth-child(1)').html("").prepend($(keyword1).hide().delay(100).fadeIn('slow').delay(2000).removeClass('zoomEffect').hide().fadeOut('slow')).addClass('zoomEffect');
				var imageLink1 = "<img src='/wp-content/uploads/2021/08/" + imgs2[Math.floor(Math.random() * imgs2.length)] + ".webp'>";
                $('.custom-animation-1.first div div:nth-child(2)').html("").prepend($(imageLink1).hide().delay(100).fadeIn('slow').delay(2000).hide().fadeOut('slow'));
				var imageLink2 = "<img src='/wp-content/uploads/2021/08/" + imgs1[Math.floor(Math.random() * imgs1.length)] + ".webp'>";
                $('.custom-animation-1.second div div:nth-child(1)').html("").prepend($(imageLink2).hide().delay(100).fadeIn('slow').delay(2000).hide().fadeOut('slow'));
				var keyword2 = "<h1>" + kwds2[Math.floor(Math.random() * kwds2.length)] + "</h1>";
                $('.custom-animation-1.second div div:nth-child(2)').html("").prepend($(keyword2).hide().delay(100).fadeIn('slow').delay(2000).hide().fadeOut('slow'));
                window.x = 0;
        }
        else{
				var imageLink3 = '<img src="/wp-content/uploads/2021/08/' + imgs2[Math.floor(Math.random() * imgs2.length)] + '.webp">';
                $('.custom-animation-1.first div div:nth-child(1)').html("").prepend($(imageLink3).hide().delay(100).fadeIn('slow').delay(2000).hide().fadeOut('slow'));
				var keyword3 = "<h1>" + kwds1[Math.floor(Math.random() * kwds1.length)] + "</h1>";
                $('.custom-animation-1.first div div:nth-child(2)').html("").prepend($(keyword3).hide().delay(100).fadeIn('slow').delay(2000).hide().fadeOut('slow'));
				var keyword4 = "<h1>" + kwds2[Math.floor(Math.random() * kwds2.length)] + "</h1>";
                $('.custom-animation-1.second div div:nth-child(1)').html("").prepend($(keyword4).hide().delay(100).fadeIn('slow').delay(2000).hide().fadeOut('slow'));
				var imageLink4 = '<img src="/wp-content/uploads/2021/08/' + imgs1[Math.floor(Math.random() * imgs1.length)] + '.webp">';
                $('.custom-animation-1.second div div:nth-child(2)').html("").prepend($(imageLink4).hide().delay(100).fadeIn('slow').delay(2000).hide().fadeOut('slow'));
                window.x = 1;
        }
    }
    setInterval(function(){ 
        aniMate(window.x);
     }, 3000);
});



/* footer @ gagan */
jQuery(document).ready(function($){
	$(".footer-nav-wrap .menu-item.menu-item-3997 i").remove();
	$(".footer-nav-wrap .menu-item.menu-item-3997 a").text('Login');
	$(".footer-nav-wrap .menu-item.menu-item-2662 i").remove();
	$(".footer-nav-wrap .menu-item.menu-item-2662 a").text('Logout');
});



/* theme scripts */
jQuery(document).ready(function($){
    new WOW().init();
	jQuery('.backtop').click(function () {
        	jQuery('html').animate({
   	             scrollTop: 0
        	},
        	3000);
	   });

	if(jQuery('.form-validate').length>0){
        jQuery('.form-validate').validate({
            errorElement: 'div',
            errorClass: 'error-field',
            invalidHandler: function (event, validator) { //display error alert on form submit
            },
            highlight: function (element) { // hightlight error inputs
                jQuery(element).closest('.form-control').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-control').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
				if(element.hasClass('errorouter')){
					error.insertAfter(element.parent());					
				}else{
					error.insertAfter(element);
				}
            },
            submitHandler: function (form, event) {
                form.submit();
            }
        });
    }
	jQuery('.jsmoredetails').on('change',function(){
		if(this.value=="yes"){
			jQuery('.jsSubmittype').val("moredetails");
		}else{
			jQuery('.jsSubmittype').val("current");			
		}
	});

});
function callbackFunc(entries, observer)
{
  entries.forEach(entry => {
	  
	  if(entry.isIntersecting){
		 $("#purp-underline").addClass("startanim"); 	
		}else{
		 $("#purp-underline").removeClass("startanim"); 			
		}
  });
}

let options = {
    root: null,
    rootMargin: '0px',
    threshold: 0.3
  };

let observer = new IntersectionObserver(callbackFunc, options);
		observer.observe(document.getElementById('purp-underline'));


