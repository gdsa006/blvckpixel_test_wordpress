<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>










<section id="pageloader"> <div class="loader"> <span style="--i:1;"></span> <span style="--i:2;"></span> <span style="--i:3;"></span> <span style="--i:4;"></span> <span style="--i:5;"></span> <span style="--i:6;"></span> <span style="--i:7;"></span> <span style="--i:8;"></span> <span style="--i:9;"></span> <span style="--i:10;"></span> <span style="--i:11;"></span> <span style="--i:12;"></span> <span style="--i:13;"></span> <span style="--i:14;"></span> <span style="--i:15;"></span> <span style="--i:16;"></span> <span style="--i:17;"></span> <span style="--i:18;"></span> <span style="--i:19;"></span> <span style="--i:20;"></span> </div> </section>

<style>

section#pageloader{
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 100vh;
    background: #042104;
    animation: animateBg 10s linear infinite;
}
@keyframes animateBg {
    0%{
        filter:hue-rotate(-224deg);
    }
    100%{
        filter: hue-rotate(360deg);
    }
}
section#pageloader .loader{
    position: relative;
    width: 120px;
    height: 120px;

}
section#pageloader .loader span{
    position:absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    transform: rotate(calc(18deg * var(--i)));
}
section#pageloader .loader span::before{
    content: '';
    position:absolute;
    top: 0;
    left: 0;
    width: 15px;
    height: 15px;
    background: #00ff0a;
    box-shadow: 0 0 10px #00ff0a,
                0 0 20px #00ff0a,
                0 0 40px #00ff0a,
                0 0 60px #00ff0a,
                0 0 80px #00ff0a,
                0 0 100px #00ff0a;
    border-radius: 50%;
    animation: animate 2s linear infinite;
    animation-delay: calc(0.1s * var(--i));
}
@keyframes animate {
    0%{
        transform: scale(1);
    }
    80%,100%{
        transform: scale(0);
    }
}

#pageloader {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  opacity: .95;
}
</style>






<?php astra_content_bottom(); ?>
	</div> <!-- ast-container -->
	</div><!-- #content -->
<?php 
	astra_content_after();
		
	astra_footer_before();
		
	astra_footer();
		
	astra_footer_after(); 
?>
	</div><!-- #page -->












<script type="text/javascript">
    /* Styles was set to !important so focefully applied via jQuery @ gagan */
jQuery(document).ready(function($) {
	$("#dae-shortcode3079-download-wrapper .dae-shortcode-download-button").attr('style', 'display: none !important');
	$("#dae-shortcode8736-download-wrapper .dae-shortcode-download-button").attr('style', 'display: none !important');
	$(".dae-shortcode-download-wrapper .dae-shortcode-register-wrapper").attr('style', 'display: block !important');
	$(".dae-shortcode-download-wrapper .dae-shortcode-download-title").attr('style', 'display: none !important');
	var categories = $('.ast-single-post.ast-blog-single-style-1 .entry-meta').find('.cat-links');
	$('.ast-single-post.ast-blog-single-style-1 .entry-meta').contents().remove();
	$('.ast-single-post.ast-blog-single-style-1 .entry-meta').html(categories);
	$(".ast-single-post.ast-blog-single-style-1 .entry-meta .cat-links").attr('style', 'position: absolute;left: 25px;bottom: 25px;');
	$("#dae-shortcode3079-download-wrapper .dae-shortcode-register-submit").attr('style', 'background-color: transparent !important; padding: 14px 35px !important; color: #ffffff !important; font-weight: normal !important; border-radius: 0 !important; width: 100% !important; text-align: center !important; font-family: "Futura LT Book", Sans-serif !important;');
	$("#dae-shortcode8736-download-wrapper .dae-shortcode-register-submit").attr('style', 'background-color: transparent !important; padding: 14px 35px !important; color: #ffffff !important; font-weight: normal !important; border-radius: 0 !important; width: 100% !important; text-align: center !important; font-family: "Futura LT Book", Sans-serif !important;');
	$("#popmake-3087 .pum-title").attr('style', 'display: none !important;');
	$("#popmake-3087 table, td, th").attr('style', 'border: 0 !important;');
	$(".dae-shortcode-download-content-wrapper").prepend( "<img src='/wp-content/uploads/2021/08/mail-icon.webp' width='140' style='width: 95px !important; margin-bottom: 10px !important;'>" );
	$("#dae-shortcode3079-download-wrapper .dae-shortcode-register-label").attr('style', 'color: #fafafa !important; font-family: "Futura LT Book", Sans-serif !important;');
	$("#dae-shortcode8736-download-wrapper .dae-shortcode-register-label").attr('style', 'color: #fafafa !important; font-family: "Futura LT Book", Sans-serif !important;');
	$("#dae-shortcode3079-download-wrapper .dae-shortcode-register-field").attr('style', 'font-family: "Futura LT Book", Sans-serif !important;');
	$("#dae-shortcode3079-download-wrapper .dae-shortcode-register-submit").hover(function(){
		$(this).attr('style', 'background-color: transparent !important; padding: 14px 35px !important; color: #a944fc !important; font-weight: normal !important; border-radius: 0 !important; width: 100% !important; text-align: center !important; font-family: "Futura LT Book", Sans-serif !important;');
	}, function(){
		$(this).attr('style', 'background-color: transparent !important; padding: 14px 35px !important; color: #ffffff !important; font-weight: bold !important; border-radius: 0 !important; width: 100% !important;text-align: center !important; font-family: "Futura LT Book", Sans-serif !important;');
	});
	$(".dae-shortcode-download-wrapper .dae-shortcode-register-input-wrap").attr('style', 'max-width: none !important; border-radius: 0 !important;  background: #f9f9f9 !important ; border: solid 1px #d0d0d0 !important;');
	$(".dae-shortcode-download-wrapper .dae-shortcode-register-icon").attr('style', 'display: none !important;');
	$(".um-register .um-half input,.um-register .um-half a").attr('style', 'font-size: 1rem !important;');
	$(".um-login .um-half input,.um-login .um-half a").attr('style', 'font-size: 1rem !important;');
	$(".page-id-2654 form input[type=text]").attr('style', 'border-left: 0 !important; border-top: 0 !important; border-right: 0 !important; padding-bottom: 15px !important; font-size: 22px !important; height: 100% !important; margin-top: 30px !important;'); //account page
	$(".page-id-2654 form input[type=text]#user_login").attr('placeholder', 'Username'); //account page 
	$(".page-id-2654 form input[type=text]#first_name").attr('placeholder', 'First Name'); //account page 
	$(".page-id-2654 form input[type=text]#last_name").attr('placeholder', 'Last Name'); //account page 
	$(".page-id-2654 form input[type=text]#user_email").attr('placeholder', 'Email Address'); //account page 
	$(".page-id-2654 form .um-field-label label").hide();
	$(".page-id-2654 form input[type=submit]#um_account_submit_general").attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 9px 35px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important;'); //account page 
	$(".page-id-2654 form input[type=submit]#um_account_submit_general").hover(function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #a944fc !important; padding: 9px 35px !important; color: #a944fc !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important; transition: 100ms;');
	}, function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 9px 35px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important;');
	}); //account page
	$(".um .um-form input[type=text], .um .um-form input[type=search], .um .um-form input[type=tel], .um .um-form input[type=number], .um .um-form input[type=password], .um .um-form textarea, .um .upload-progress, .select2-container .select2-choice, .select2-drop, .select2-container-multi .select2-choices, .select2-drop-active, .select2-drop.select2-drop-above.um .um-form input[type=text], .um .um-form input[type=search], .um .um-form input[type=tel], .um .um-form input[type=number], .um .um-form input[type=password], .um .um-form textarea, .um .upload-progress, .select2-container .select2-choice, .select2-drop, .select2-container-multi .select2-choices, .select2-drop-active, .select2-drop.select2-drop-above").attr('style', 'border-left: 0 !important; border-top: 0 !important; border-right: 0 !important; padding-bottom: 15px !important; font-size: 22px !important; height: 100% !important; margin-top: 30px !important;'); //account page
	$(".page-id-2654 form input[type=password]#current_user_password").attr('placeholder', 'Current Password'); //account page 
	$(".page-id-2654 form input[type=password]#user_password").attr('placeholder', 'New Password'); //account page
	$(".page-id-2654 form input[type=submit]#um_account_submit_password").attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 9px 35px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important;'); //account page 
	$(".page-id-2654 form input[type=submit]#um_account_submit_password").hover(function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #a944fc !important; padding: 9px 35px !important; color: #a944fc !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important; transition: 100ms;');
	}, function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 9px 35px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important;');
	}); //account page
	$(".page-id-2654 .um .um-form input[type=text], .um .um-form input[type=search], .um .um-form input[type=tel], .um .um-form input[type=number], .um .um-form input[type=password], .um .um-form textarea, .select2-container .select2-choice, .select2-container-multi .select2-choices").attr('style', 'background-color: transparent !important; border-left: 0 !important; border-top: 0 !important; border-right: 0 !important; padding-bottom: 15px !important; font-size: 22px !important; height: 100% !important; margin-top: 30px !important;'); //account page
	$(".page-id-2654 .um-account-tab-privacy input[type=password]").attr('placeholder', 'Password'); //account page
	$(".page-id-2654 form input[type=submit]#um_account_submit_privacy").attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 9px 35px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important;'); //account page 
	$(".page-id-2654 form input[type=submit]#um_account_submit_privacy").hover(function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #a944fc !important; padding: 9px 35px !important; color: #a944fc !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important; transition: 100ms;');
	}, function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 9px 35px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important;');
	}); //account page
		$(".page-id-2654 .select2.select2-container .select2-selection").attr('style', 'background-color: black !important; margin-bottom: 30px; color: #fff !important;  border-radius: 0px !important; font-size: 1rem !important; border-top: 0 !important; border-left: 0 !important; border-right: 0 !important '); //account page 
		$(".page-id-2654 .select2-container--default .select2-selection--single .select2-selection__rendered").attr('style', 'color: #ffffff !important; '); //account page 	
	$(".um-field-area .um-field-radio .um-icon-android-radio-button-on").attr('style', 'color: #a944fc !important;'); //account page 
	$(".um-field-area .um-field-radio.active .um-icon-android-radio-button-on").attr('style', 'color: #a944fc !important;'); //account page 
	$(".page-id-2654 form #um_field_privacy_hide_in_members .um-field-label label").show(); //account page 
	$(".page-id-2654 form #um_field_privacy_profile_privacy .um-field-label label").show(); //account page 
	$(".page-id-2654 form #um_field_privacy_profile_noindex .um-field-label label").show(); //account page 
	$(".page-id-2654 form .um-field-export_data .um-field-label label").show(); //account page 
	$(".page-id-2654 form #um_field_privacy_hide_in_members").attr('style', 'margin-bottom: 30px'); //account page 
	$(".page-id-2654 form .um-field-export_data").attr('style', 'margin-bottom: 30px'); //account page 
	$(".page-id-2654 form .um-field-export_data a.um-request-button").attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 5px 10px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 0.8rem !important;'); //account page  
	$(".page-id-2654 form .um-field-export_data a.um-request-button").hover(function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #a944fc !important; padding: 5px 10px !important; color: #a944fc !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 0.8rem !important; transition: 100ms;'); //account page 
	}, function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 5px 10px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 0.8rem !important;'); //account page 
	}); //account page
	$(".page-id-2654 .um-account-tab-delete input[type=password]").attr('placeholder', 'Password'); //account page
	$(".page-id-2654 form input[type=submit]#um_account_submit_delete").attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 9px 35px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important;'); //account page 
	$(".page-id-2654 form input[type=submit]#um_account_submit_delete").hover(function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #a944fc !important; padding: 9px 35px !important; color: #a944fc !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important; transition: 100ms;');
	}, function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 9px 35px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important;'); //account page 
	}); //account page
	$(".page-id-2654 .um-account-side li").attr('style', 'background-color: black !important;'); //account page 
	$(".page-id-2654 .um-account-side .um-account-link.current").attr('style', 'background-color: black !important;'); //account page 
		$current_logged_in_user = $(".page-id-2654 .um-account-tab-unsubscribe_newsletters #hidden-email-unsubscribe").val(); //account page 
		$(".page-id-2654 .um-account-tab-unsubscribe_newsletters .mc4wp-form-fields input[type=email]").attr("value", $current_logged_in_user);
		$MailChimpUnsubscribeHiddenInput = '<input type="hidden" name="_mc4wp_action" value="unsubscribe" />';
		$(".page-id-2654 .um-account-tab-unsubscribe_newsletters .mc4wp-form-fields p").prepend($MailChimpUnsubscribeHiddenInput);
		$(".page-id-2654 .um-account-tab-unsubscribe_newsletters .mc4wp-form-fields input[type=submit]").attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 9px 35px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important;'); //account page 
		$(".page-id-2654 .um-account-tab-unsubscribe_newsletters .mc4wp-form-fields input[type=submit]").attr('value', 'Unsubscribe'); //account page 
	$(".page-id-2654 .um-account-tab-unsubscribe_newsletters .mc4wp-form-fields input[type=submit]").hover(function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #a944fc !important; padding: 9px 35px !important; color: #a944fc !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important; transition: 100ms;');
	}, function(){
		$(this).attr('style', 'background-color: black; color: white; border: 1px solid #fff !important; padding: 9px 35px !important; color: #fff !important; margin-top: 30px !important; border-radius: 0px !important; font-size: 1rem !important;');
	}); //account page
	$(".page-id-2654 .um-account-tab-unsubscribe_newsletters #um_account_submit_unsubscribe_newsletters").hide(); //account page 
	$(".page-id-2654 .um-account-tab-unsubscribe_newsletters .mc4wp-form-fields input[type=email]").hide(); //account page 
	$(".page-id-2654 .um-account-main .um-account-tab div:nth-child(2) .um-field-area input").css('margin-top', '0');
	$(".page-id-2654 .um-account-main .um-account-tab div:nth-child(2) .um-field-area input").css('margin-top', '0');
	$("header #ast-hf-menu-1 .astm-search-menu").attr('class', 'astm-search-menu is-menu is-dropdown menu-item menu-item-type-post_type menu-item-object-page menu-item-000');
	$("header #ast-hf-menu-1 .astm-search-menu a").attr('class', 'menu-link').attr('style','line-height: inherit !important');
	$("header #ast-hf-menu-1 .astm-search-menu a svg.search-icon").attr('style','background-color: transparent; padding: unset !important; top: 4px; position: relative; left: -6px;');
	$("header #ast-hf-menu-1 .astm-search-menu a svg.search-icon path").attr('style','fill: #ffffff !important');
	$(".page-id-2654 .um-account-tab-unsubscribe_newsletters .mc4wp-form-fields input[type=submit]").attr('value', 'Unsubscribe'); //account page 
	$("header .astm-search-menu").hover(function(){
		$("header .astm-search-menu a svg.search-icon path").attr('style', 'fill: #a944fc !important');
	}, function(){
		$("header .astm-search-menu a svg.search-icon path").attr('style', 'fill: #ffffff !important');
	}); //account page
	$("body.search .post-content.ast-grid-common-col, body.archive.tag .post-content.ast-grid-common-col, body.archive.category .post-content.ast-grid-common-col").addClass('row');
	$("body.search .post-content.ast-grid-common-col .ast-blog-featured-section, body.archive.tag .post-content.ast-grid-common-col .ast-blog-featured-section, body.archive.category .post-content.ast-grid-common-col .ast-blog-featured-section").addClass('col-xs-12 col-md-6');
	$("body.search .post-content.ast-grid-common-col header.entry-header, body.archive.tag .post-content.ast-grid-common-col header.entry-header, body.archive.category .post-content.ast-grid-common-col header.entry-header").addClass('wrapMe');
	$("body.search .post-content.ast-grid-common-col div.entry-content, body.archive.tag .post-content.ast-grid-common-col div.entry-content, body.archive.category .post-content.ast-grid-common-col div.entry-content").addClass('wrapMe');
	$("body.search main article").each(function(index) {
		$("body.search article:nth-child(" + (index + 1) + ") .post-content.ast-grid-common-col .wrapMe").wrapAll( "<div class='col-xs-12 " + index + " col-md-6 '></div>" ); //justify-content-center align-self-center
	})
	$("body.archive.tag main article").each(function(index) {
		$("body.archive.tag article:nth-child(" + (index + 1) + ") .post-content.ast-grid-common-col .wrapMe").wrapAll( "<div class='col-xs-12 " + index + " col-md-6 '></div>" ); //justify-content-center align-self-center
	})
	$("body.archive.category main article").each(function(index) {
		$("body.archive.category article:nth-child(" + (index + 1) + ") .post-content.ast-grid-common-col .wrapMe").wrapAll( "<div class='col-xs-12 " + index + " col-md-6 '></div>" ); //justify-content-center align-self-center
	})
});
</script>



<?php 
	astra_body_bottom();    
	wp_footer(); 
?>



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">



 
<!-- <script>
      particlesJS.load('particle-js', '<?= get_template_directory_uri() . '/assets/package.json' ?>', function() {
  });
</script> -->



 

	</body>
</html>
