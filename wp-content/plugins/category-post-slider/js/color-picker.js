(function( $ ) {
 
    // Add Color Picker to all inputs that have 'color-field' class
    $(function() {
        $('.cps-color-picker').wpColorPicker();
    });
     
})( jQuery );